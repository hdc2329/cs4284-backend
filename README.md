# CS4284-BackEnd

Code for the backend machines for Team Diamond Hands, CS 4284, Spring 2021

# Getting Development Environment Running

1. Install docker, and [compose](https://docs.docker.com/compose/install/)
2. Clone this repository
3. To build the image run `sudo docker-compose build`
4. To run the image run `sudo docker-compose up`. 
5. To stop an image, use `Ctrl+c` or `sudo docker-compose down` in another shell.

- `website/` contains the django code.
- Items in the `content/` directory will be served from `static/` by the server. _Git does not track this directory._
- Changes inside the repository should translate immediately to a running container without need for a restart.

# Changes Needed To Deploy

- Serve static files [properly](https://docs.djangoproject.com/en/3.1/howto/static-files/deployment/)
- WSGI server (not django debug mode)
- auth
- signed urls?
