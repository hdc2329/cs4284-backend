import json
import time
import subprocess
import requests
import re
from pathlib import Path

DEBUG = True

USER_POS = 0 
SYS_POS = 1
IDLE_POS = 2 
count = 1
prev = 0

MASTER_URL = "http://meltdown.vtluug.org:8000"

# maybe need this for CSRF stuff...
client = requests.session()

def debug(s):
	if DEBUG:
		print(s)


def get_energy():
	try:
		data_folder = Path("/sys/class/powercap/intel-rapl/intel-rapl:0/energy_uj")
		f = open(data_folder)
		energy = int(f.read())
		f.close()
		return energy
	except FileNotFoundError:
		return 0


def get_power(energy, count):
	return energy / count


def get_cpu_utilization():
	with open("top-output.txt", "w") as outfile:
		subprocess.call("top -n1" , shell=True, stdout=outfile)

	with open("top-output.txt", "r") as infile:
		contents = infile.read().split('\n')
		reaesc = re.compile(r'\x1b[^m]*m')
		stats_str = contents[2].split('%Cpu(s):')[1].strip()
		remove_ansi = reaesc.sub('', stats_str)
		stats = remove_ansi.strip().split(',')
		ret = {
			'user' :	stats[0].strip().split(' ')[0],
			'system':	stats[1].strip().split(' ')[0],
			'idle' :	stats[3].strip().split(' ')[0]
		}
				
		#debug(ret)
		return ret
	


def get_power_info(baseline, prev, count):
	energy = get_energy() - baseline
	power = get_power(energy, count)
	ret = {
		'uJ' : energy - prev,
		'uW' : power,
		'uJ-total' : energy
		}
	return ret	


baseline = get_energy()


# Stuff to get CSRF Working
def getCSRFToken():
	response = client.get(MASTER_URL + "/csrf/")
	token = response.json()['csrfToken']
	return token

endpoint = MASTER_URL + "/master/upload"
token = getCSRFToken()


while True:
	post_data = {'ipv4': '10.98.0.4', 'csrfmiddlewaretoken': token}
	post_data.update(get_cpu_utilization())
	post_data.update(get_power_info(baseline, prev, count))
	debug(post_data)
	#response = requests.post(MASTER_URL + "/stats/upload/", data=post_data)
	r = client.post(endpoint, data=post_data, headers=dict(Referer=endpoint))
	debug(r.text)
	debug("")
	prev = post_data['uJ-total']
	count += 1
	time.sleep(1)
