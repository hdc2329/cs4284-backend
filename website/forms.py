from django import forms
from .models import *

class ImageForm(forms.ModelForm):

    class Meta:
        model = Image_Job
        fields = ['name','job_image']
