from django.db import models


class Nodes(models.Model):
	fqdn = models.CharField(max_length=200)
	ipv4 = models.CharField(max_length=200)
	ipv6 = models.CharField(max_length=200)
	base_worker_port = models.IntegerField(default=8000)                      
	current_worker = models.IntegerField(default=8000)
	highest_worker = models.IntegerField(default=8004)

	# To support JSON.dumps encoding
	def toJson(self):
		return {
			'fqdn': self.fqdn,
			'ipv4': self.ipv4,
			'ipv6': self.ipv6,
			'base_worker_port': self.base_worker_port,
			'current_worker': self.current_worker,
			'highest_worker': self.highest_worker
		}

	def __str__(self):
		return self.fqdn + ' ' +self.ipv4 + ' ' + self.ipv6 


class Statistics(models.Model):
	nodeID = models.ForeignKey(Nodes, on_delete=models.CASCADE)
	time = models.DateTimeField(auto_now_add=True)
	micro_watts = models.FloatField(default=0)
	micro_joules= models.FloatField(default=0)
	cumulative_micro_joules = models.FloatField(default=0)
	user = models.FloatField(default=0)
	system = models.FloatField(default=0)
	idle = models.FloatField(default=0)
	jobs_completed = models.IntegerField(default=0)

	# To support JSON.dumps encoding
	def toJson(self):
		return { 
			'nodeID': self.nodeID.toJson(),
			'time': self.time,
			'micro_watts': self.micro_watts,
			'micro_joules': self.micro_joules,
			'cumulative_micro_joules': self.cumulative_micro_joules,
			'user_utilization': self.user,
			'system_utilization': self.system,
			'idle_utilization': self.idle,
			'jobs_completed': self.jobs_completed
		}


'''
    class that represents a round robin load balancer, which for 
	2-way load balancing, is just a boolean. This can be turned into
	an n-way load balancer by making the switch an integer and adding
	a maxSize field, then add a method that increments the switch 
	correctly to wrap around a max value and distribute jobs in a 
	round robin fashion
	''' 
class RoundRobinLoadBalancer(models.Model):
	lb_name = models.CharField(max_length=100,default="rrlb")
	switch = models.BooleanField(default=False)

	def __str__(self):
		return self.lb_name + ' ' + str(self.switch)