from django.urls import path

from . import views

urlpatterns = [
    path('upload', views.stats_upload, name='stats-upload'),
    path('lb', views.power_lb,name='power-lb'),
    path('statistics', views.get_stats, name='get-stats'),
    path('add-stats', views.add_stats, name='add-stats'),
]
