from django.shortcuts import render
from django.shortcuts import HttpResponse
from django.shortcuts import redirect
from django.http import JsonResponse
from .models import *
import json


def stats_upload(request):
	if request.method == "POST":
		body = request.POST
		try:
			# Find the associated node
			ipv4 = body['ipv4']
			query = list(Nodes.objects.filter(ipv4=ipv4).all())
			if len(query) != 1:
				raise Exception('Database has multiple nodes storing the same ip address')
			node = query[0]
			
			# Build Statistics DB Object
			s = Statistics(
				nodeID = node,
				micro_watts = body['uW'],
				micro_joules = body['uJ'],
				cumulative_micro_joules = body['uJ-total'], 
				user = body['user'],
				system = body['system'],
				idle = body['idle']				
				)
			s.save()

		except KeyError:
			return HttpResponse('Error: Missing Required Fields', status=400)
		except:
			return HttpResponse('Error: could not upload statistic from worker node, remember to chmod 777 website and db.sqlite3', status=400)

		# Uncomment below for more insightful debuggers
		"""
		# Find the associated node
		ipv4 = body['ipv4']
		query = list(Nodes.objects.filter(ipv4=ipv4).all())
		if len(query) != 1:
			raise Exception('Database has multiple nodes storing the same ip address')
		node = query[0]
		
		# Build Statistics DB Object
		s = Statistics(
			nodeID = node,
			micro_watts = body['uW'],
			micro_joules = body['uJ'],
			cumulative_micro_joules = body['uJ-total'], 
			user = body['user'],
			system = body['system'],
			idle = body['idle']				
			)
		s.save()
		"""
		return HttpResponse('Success')


'''
	method that actually performs the power load balancing: when a request comes in,
	search the statistics database for the one with the lowest power, then get the node
	associated with that statistics and return that node, or redirect to that node or
	whatever
'''
def power_lb(request):
	#get all the nodes from the database and put them in a list
	node_list = list(Nodes.objects.all())
	#get the most recent statistic for each node
	min_power_node = node_list[0]
	min_power_stat = min_power_node.statistics_set.latest('time')
	for item in node_list:
		lastest_stat = item.statistics_set.latest("time")
		if lastest_stat.micro_watts < min_power_stat.micro_watts:
			min_power_node = item
			min_power_stat = lastest_stat
	'''
		call the helper to figure out the next worker on the node to get the
		job, then use that return value to build the first part of the url 
		for the redirect or whatever
	'''
	# Ignore this until we get multiple containers within a single node working
	# full_redirect_destination = min_power_node.fqdn + select_and_increment_worker(min_power_node)
	# test_value = full_redirect_destination
	
	# NOTE: SPECTRE IS DOWN ATM (5/5/2021)
	upload_endpoint = "http://meltdown.vtluug.org:8000" + "/upload/"
	#upload_endpoint = min_power_node.fqdn + "/upload/"
	return HttpResponse(upload_endpoint)

	
'''
	assign the task to the least recently used worker on the node selected (
	which is just assigned in a round robin fashion by port number) and
	increments the current worker value appropriately, then returns a string
	that is the port portion of the redirect destination
'''
def select_and_increment_worker(node):
	assigned_worker = node.current_worker
	node.current_worker += 1
	if node.current_worker > node.highest_worker:
		node.current_worker = node.base_worker_port
	node.save()
	return ":" +str(assigned_worker)



def get_stats(request):
	if request.method == "GET":
		testing = Statistics.objects
		entry_list = list(testing.all())
		
		output = [x.toJson() for x in entry_list][-100:]

		return HttpResponse(json.dumps(output, default=str))
	return HttpResponse('Error: get_stats does not accept anythign else but get requests', status=400)



"""
Hidden endpoint to add the nodes we're using.
"""
def add_stats(request):
	meltdown_node = Nodes(
		fqdn='http://meltdown.vtluug.org:8000',
		ipv4='128.173.89.246',
		ipv6='2607:b400:6:cc80:0:aff:fe62:3'
	)

	# Faking data for this node since it's stuck behind VT's proxy?
	spectre_node = Nodes(
			fqdn='http://spectre.vtluug.org:8000', #change this to local if needed
			ipv4='10.98.0.4',
			ipv6='2607:b400:6:cc80:0:aff:fe62:4'
	)

	try:
		# NOTE: REMEMBER TO CLEAR DB, OTHERWISE WE'RE GOING TO HAVE IDENTITICAL NODES
		Nodes.objects.all().delete()
		meltdown_node.save()
		spectre_node.save()
	except:
		return HttpResponse('Error: Could not insert into database', status=400)
	return HttpResponse('Good')


"""
Parse IP Address from a HTTP Request
"""
def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip
