from django.db import models

# Create your models here.

#a model for an image job so we can get the uploaded image and store it for manipulation 
class Image_Job(models.Model):
    name =models.CharField(max_length=50)
    job_image = models.ImageField(upload_to='images/')
