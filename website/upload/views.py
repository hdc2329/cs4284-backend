from django.shortcuts import render
from django.core.files.temp import NamedTemporaryFile

# Create your views here.
from django.conf import settings
from django.core.files.storage import default_storage
from django.http import FileResponse
from django.http import HttpResponse
from django.shortcuts import redirect
import json
import sys
import subprocess

"""
    Entry point of requests at '/upload' endpoint

    Request.POST Body will be of structure {
        size: int (image size in bytes)
        name: string (name of file)
        type: string (file extension)
        action: string (action to be taken e.g 'flip' / 'flop' 'rotate')
        dimensions: dict {
            width: int
            height: int 
        }
    }

    Request.FILES Body will contain a structure of {
        image-file: File (Image file that the user uploaded)
    }

    NOTE: MORE WILL BE ADDED/UPDATED AS WE SUPPORT THE MORE COMPLEX IMAGE MANIPULATIONS
"""
def index(request):
    if request.method == "POST":
        # Parse body of Post Request
        size = request.POST['size']
        name = request.POST['name']
        file_type = request.POST['type']
        action = request.POST['action']
        dimensions = request.POST['dimensions']

        # Retrieve file and save it to local storage for manipulation
        image_file = request.FILES['image-file']
        image_name = default_storage.save(image_file.name, image_file)
        #image_name = NamedTemporaryFile(image_file.name, image_file)
        ## TODO: Add input quality check in case of malicious attacks

        try:
            legalActions = ['flip','flop','rotate','grayscale','convert']
            if action in legalActions:
                no_argument_manipulation(image_name, action)
            else:
                default_storage.delete(image_name)
                return HttpResponse('Action currently not supported')

            # Grab manipulated file from storage and return to client
            result_img = default_storage.open(image_name)
            '''
            save the image into the response, then delete the image and 
            return the response. Does this work for large files? It certainly works with the 17mb image
            '''
            response = FileResponse(result_img)
            default_storage.delete(image_name)
            #don't use this, need result_img already in the response so the file can be deleted
            #return FileResponse(result_img)
            return response
        except:
            default_storage.delete(image_name)
            return HttpResponse('Something went wrong on the server side of things')
        #I think there's a way to do the file delete cleanly in a finally block, but I need
        #to make it work with the if-else at the top which doesn't throw an exception I think
            
    else:
        return HttpResponse("That was not a post Request")
        
# method for performing one of the argument-less image manipulations (flip, flop)        
def no_argument_manipulation(file_name, action):
    """
    NOTE: Resulting image file will replace the original file
    """

    # Correctly configure path as subprocess uses 'website/media/[file]' whereas Django stores files at '/code/website/media/[file]'
    django_path = settings.MEDIA_ROOT
    full_image_path = django_path[django_path.index('website/'):] + '/' + file_name

    
    # Format action for ImageMagick input
    action_command = "-" + action

    # Build argv for running command
    command = ["./magick", "--appimage-extract-and-run", full_image_path, action_command, full_image_path]
    
    #add the required argument for grayscale
    if action == 'grayscale':
        command.insert(4 , 'Rec709Luminance')
    # execute the command. If the process returns anything besides 0, we'll get a CalledProcessError
    try:
        subprocess.run(command, check=True)
    except:
        e = sys.exc_infor()[0]
        print(e)
        raise Exception("Error with Running ImageMagick Command!")
