"""website URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.urls import re_path

#static url serving
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic.base import TemplateView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('csrf/', include('csrf.urls')),
    path('upload/', include('upload.urls')),
	path('stats/', include('master.urls')),
    path('master/',include('master.urls'))		#is it possible to have separate url files included so we can make the endpoints different?
] 


#TODO: FOR DEVELOPMENT ONLY. Configure serving static files from the web-server in deployment
if settings.DEBUG:
	# serve static content (in STATIC_ROOT) at /static
	urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
	#adding support for the media directory
	urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
	# serve index.html afrom /



if settings.DEBUG:
	"""
    urlpatterns += re_path( 
        r'^$', 'django.contrib.staticfiles', kwargs={
            'path': 'public/index.html', 'document_root': settings.STATIC_ROOT})

	urlpatterns += re_path(r'^$', TemplateView.as_view(template_name='index.html'))
	"""
